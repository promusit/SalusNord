﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using RestSharp.Authenticators;

namespace WebAPI.Helpers
{
    public static class NotifyClient
    {
        private static RestClient client = new RestClient();
        private static readonly Uri baseUrl = new Uri("https://api.mailgun.net/v3/");
        private static readonly string password = "key-777afbfced6716d0ecf90d57a3dfebf4";


        public static IRestResponse Notify(string resident, string toEmail, string toName)
        {
            client.BaseUrl = baseUrl;
            client.Authenticator = new HttpBasicAuthenticator("api", password);

            RestRequest request = new RestRequest();
            request.AddParameter("domain", "sandbox7e8737aa20ed4d2783bea765cc79cb6a.mailgun.org", ParameterType.UrlSegment);
            request.Resource = "sandbox7e8737aa20ed4d2783bea765cc79cb6a.mailgun.org/messages";

            request.AddParameter("from", "Salus Nord <mailgun@sandbox7e8737aa20ed4d2783bea765cc79cb6a.mailgun.org>");
            request.AddParameter("to", toEmail);
            request.AddParameter("subject", resident + " har forladt institutionen.");
            request.AddParameter("html", "" +
                "<html>" +
                "<p>Hej " + toName + "</p>" +
                "<p>" + resident + " har forladt plejehjemmet klokken " + DateTime.Now.ToLongTimeString() + ".</p>" +
                "<p>Venlig hilsen</p>" +
                "<p>Salus Nord IVS </br>Grundtvigsvej 13</br>9600 Aars</p>" +
                "<p>Telefon: 42773382</br>Email: info@salusnord.dk</p>" +
                "</html>");

            request.Method = Method.POST;
            var response = client.Execute(request);

            return response;
            
        }
    }
}