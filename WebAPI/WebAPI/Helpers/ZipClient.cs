﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO.Compression;
using System.IO;
using System.Threading.Tasks;

namespace WebAPI.Helpers
{
    public static class ZipClient
    {
        public async static Task Decompress(string filePath, string uploadPath)
        {
            await Task.Run(() => ZipFile.ExtractToDirectory(filePath, uploadPath));
        }
    }
}