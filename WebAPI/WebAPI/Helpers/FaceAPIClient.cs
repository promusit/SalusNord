﻿
using Microsoft.Azure.CognitiveServices.Vision.Face;
using Microsoft.Azure.CognitiveServices.Vision.Face.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using WebAPI.Models;

namespace WebAPI.Helpers
{
    public class FaceAPIClient
    {
        private const string subscriptionKey = "0d06afa004eb48cf8e5397fe7316e810";
        private const string baseUri = "https://northeurope.api.cognitive.microsoft.com";

        private readonly IFaceClient faceClient = new FaceClient(
            new ApiKeyServiceClientCredentials(subscriptionKey),
            new System.Net.Http.DelegatingHandler[] { });

        public FaceAPIClient()
        {
            if (Uri.IsWellFormedUriString(baseUri, UriKind.Absolute))
            {
                faceClient.Endpoint = baseUri;
            }
        }

        public async Task<TrainingStatusType> TrainPersonGroup(string personGroupId)
        {
            TrainingStatus trainingStatus = null;

            try
            {
                //PersonGroup personGroup = await GetOrCreatePersonGroup(personGroupId);
                PersonGroup personGroup = await faceClient.PersonGroup.GetAsync(personGroupId);
                if (personGroup != null)
                {
                    await faceClient.PersonGroup.TrainAsync(personGroupId);

                    while (true)
                    {
                        trainingStatus = await faceClient.PersonGroup.GetTrainingStatusAsync(personGroupId);
                        if (trainingStatus.Status != TrainingStatusType.Running)
                        {
                            break;
                        }

                        await Task.Delay(500);
                    }

                    return trainingStatus.Status;
                }
                else
                {
                    //Temporary
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                throw;
            }


        }

        public async Task CreatePersonGroupPerson(string personGroupId, int id, string name, string room, string directory)
        {
            string strId = id.ToString();
            Person person = await faceClient.PersonGroupPerson.CreateAsync(personGroupId, name = strId);

            foreach (string path in Directory.GetFiles(directory, "*.jpg"))
            {
                using (var stream = File.OpenRead(path))
                {
                    await faceClient.PersonGroupPerson.AddFaceFromStreamAsync(personGroupId, person.PersonId, stream);
                }
            }

            return;
        }

        private async Task<PersonGroup> GetOrCreatePersonGroup(string personGroupId)
        {
            var listOfGroups = await faceClient.PersonGroup.ListAsync();

            if (listOfGroups != null)
            {
                if ((listOfGroups.FirstOrDefault(x => x.PersonGroupId == personGroupId)) == null)
                {
                    await faceClient.PersonGroup.CreateAsync(personGroupId, personGroupId);
                }
                else
                {
                    //Testing Purposes:
                    await faceClient.PersonGroup.DeleteAsync(personGroupId);
                    await faceClient.PersonGroup.CreateAsync(personGroupId, personGroupId);
                }
            }
            else
            {
                await faceClient.PersonGroup.CreateAsync(personGroupId, personGroupId);
            }

            return await faceClient.PersonGroup.GetAsync(personGroupId);
        }

        public async Task<Person> CompareImage(string imagePath, string nursingHome)
        {
            // Detecting face on uploaded picture.
            var detectedFace = await UploadAndDetectFace(imagePath);

            var faceIds = detectedFace.Select(face => (Guid)face.FaceId).ToArray();

            var results = await faceClient.Face.IdentifyAsync(faceIds, nursingHome);
            Person matchingPerson = null;

            foreach (var result in results)
            {
                if (result.Candidates.Count != 0)
                {
                    Guid matchingPersonId = result.Candidates[0].PersonId;
                    matchingPerson = await faceClient.PersonGroupPerson.GetAsync(nursingHome, matchingPersonId);

                }
            }

            return matchingPerson;
        }

        private async Task<IList<DetectedFace>> UploadAndDetectFace(string imagePath)
        {
            try
            {
                using (Stream stream = File.OpenRead(imagePath))
                {
                    var faces = await faceClient.Face.DetectWithStreamAsync(
                        stream, true, true, new FaceAttributeType[]
                        {
                            FaceAttributeType.Accessories,
                            FaceAttributeType.Age,
                            FaceAttributeType.Blur,
                            FaceAttributeType.Emotion,
                            FaceAttributeType.Exposure,
                            FaceAttributeType.FacialHair,
                            FaceAttributeType.Gender,
                            FaceAttributeType.Glasses,
                            FaceAttributeType.Hair,
                            FaceAttributeType.HeadPose,
                            FaceAttributeType.Makeup,
                            FaceAttributeType.Noise,
                            FaceAttributeType.Occlusion,
                            FaceAttributeType.Smile
                        });

                    return faces;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task DeleteResident(string groupId, int id)
        {
            IList<Person> persons = await faceClient.PersonGroupPerson.ListAsync(groupId);
            Person personToDelete = persons.SingleOrDefault(x => x.Name == id.ToString());

            if (personToDelete != null)
            {
                try
                {
                    await faceClient.PersonGroupPerson.DeleteAsync(groupId, personToDelete.PersonId);
                    return;

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

            //await faceClient.PersonGroupPerson.DeleteAsync(groupId, id);
        }
    }
}