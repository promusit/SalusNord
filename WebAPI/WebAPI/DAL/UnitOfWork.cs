﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebAPI.Models;

namespace WebAPI.DAL
{
    public class UnitOfWork : IDisposable
    {
        private DbContextSalusNord ctx = new DbContextSalusNord();
        private GenericRepository<ApplicationUser> appUserRepository;
        private GenericRepository<Provider> providerRepository;
        private GenericRepository<NursingHome> nursingHomeRepository;
        private GenericRepository<Contact> contactRepository;
        private GenericRepository<Resident> residentRepository;
        private GenericRepository<ContactGroup> contactGroupRepository;

        private bool disposed;

        public DbContextSalusNord Context => ctx;

        public GenericRepository<ApplicationUser> AppUserRepository
        {
            get
            {
                if (appUserRepository == null)
                {
                    appUserRepository = new GenericRepository<ApplicationUser>(ctx);
                }

                return appUserRepository;
            }
        }

        public GenericRepository<Provider> ProviderRepository
        {
            get
            {
                if (providerRepository == null)
                {
                    providerRepository = new GenericRepository<Provider>(ctx);
                }

                return providerRepository;
            }
        }

        public GenericRepository<NursingHome> NursingHomeRepository
        {
            get
            {
                if (nursingHomeRepository == null)
                {
                    nursingHomeRepository = new GenericRepository<NursingHome>(ctx);
                }

                return nursingHomeRepository;
            }
        }

        public GenericRepository<Contact> ContactRepository
        {
            get
            {
                if (contactRepository == null)
                {
                    contactRepository = new GenericRepository<Contact>(ctx);
                }

                return contactRepository;
            }
        }

        public GenericRepository<Resident> ResidentRepository
        {
            get
            {
                if (residentRepository == null)
                {
                    residentRepository = new GenericRepository<Resident>(ctx);
                }

                return residentRepository;
            }
        }

        public GenericRepository<ContactGroup> ContactGroupRepository
        {
            get
            {
                if (contactGroupRepository == null)
                {
                    contactGroupRepository = new GenericRepository<ContactGroup>(ctx);
                }

                return contactGroupRepository;
            }
        }

        public async Task<int> Save()
        {
            return await ctx.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }

            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}