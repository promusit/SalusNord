﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using WebAPI.Models;

namespace WebAPI.DAL.Migrations
{
    public class Configuration : DbMigrationsConfiguration<DbContextSalusNord>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;

        }

        protected override void Seed(DbContextSalusNord context)
        {

            // Creating Roles
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            if (!context.Roles.Any(r => r.Name == "NursingHome"))
            {
                var role = new IdentityRole { Name = "NursingHome" };
                roleManager.Create(role);
            }

            if (!context.Roles.Any(r => r.Name == "Provider"))
            {
                var role = new IdentityRole { Name = "Provider" };
                roleManager.Create(role);
            }

            // Creating Users
            if (!context.Users.Any(u => u.UserName == "rnp@salusnord.dk"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                manager.UserValidator = new UserValidator<ApplicationUser>(manager) { AllowOnlyAlphanumericUserNames = false };
                var user = new ApplicationUser
                {
                    UserName = "rnp@salusnord.dk",
                    Email = "rnp@salusnord.dk",
                    Provider = new Provider
                    {
                        Name = "Rasmus Nedergaard Pedersen",
                    }
                };

                manager.Create(user, "#SalusNord2018");
                manager.AddToRole(user.Id, "Provider");
            }

            if (!context.Users.Any(u => u.UserName == "rnp@promus-it.dk"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                manager.UserValidator = new UserValidator<ApplicationUser>(manager) { AllowOnlyAlphanumericUserNames = false };
                var user = new ApplicationUser
                {
                    UserName = "rnp@promus-it.dk",
                    Email = "rnp@promus-it.dk",
                    NursingHome = new NursingHome
                    {
                        Name = "Grundtvigsvej",
                        Address = "Grundtvigsvej 13",
                        City = "Aars",
                        Zip = "9600",
                        Phone = "42773382"
                    }
                };

                manager.Create(user, "#PromusIT2018");
                manager.AddToRole(user.Id, "NursingHome");
            }









        }
    }
}