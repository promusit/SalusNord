﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace WebAPI.DAL
{
    public class GenericRepository<T> where T : class
    {
        internal DbContextSalusNord ctx;
        internal DbSet<T> dbSet;

        public GenericRepository(DbContextSalusNord ctx)
        {
            this.ctx = ctx;
            dbSet = ctx.Set<T>();

        }

        public virtual async Task<IEnumerable<T>> Get(
            Expression<Func<T, bool>> filter = null, 
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<T> query = dbSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }

            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            return orderBy != null ? await orderBy(query).ToListAsync() : await query.ToListAsync();

        }

        public virtual async Task<T> GetById(object id)
        {
            return await dbSet.FindAsync(id);
        }

        public virtual async Task<List<T>> GetAll()
        {
            return await dbSet.ToListAsync();
        }

        public virtual async Task Create(T entity)
        {
            dbSet.Add(entity);
        }

        public virtual async Task<T> Delete(object id)
        {
            T entityToDelete = await dbSet.FindAsync(id);
            await Delete(entityToDelete);
            return entityToDelete;
        }

        public virtual async Task<T> Delete(T entityToDelete)
        {
            if (ctx.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }

            return dbSet.Remove(entityToDelete);
        }

        public virtual async Task<T> Update(T entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            ctx.Entry(entityToUpdate).State = EntityState.Modified;
            return entityToUpdate;
        }
    }
}