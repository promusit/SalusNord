﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPI.Models;

namespace WebAPI.DAL
{
    public class IdentityUserManager : UserManager<ApplicationUser>
    {
        public IdentityUserManager() : base(new IdentityUserStore())
        {

        }

        public IdentityUserManager(IdentityUserStore store) : base(store)
        {

        }
    }
}