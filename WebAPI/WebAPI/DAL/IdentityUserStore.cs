﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPI.Models;

namespace WebAPI.DAL
{
    public class IdentityUserStore : UserStore<ApplicationUser>
    {
        public IdentityUserStore() : base(new DbContextSalusNord())
        {

        }

        public IdentityUserStore(DbContextSalusNord context) : base(context)
        {

        }
    }
}