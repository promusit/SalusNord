﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebAPI.Controllers
{

    public class AccountsController : ApiController
    {
        // GET: api/accounts
        public async Task<IHttpActionResult> Get()
        {
            var u = User.Identity;

            if (u == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Ingen oplysninger blev fundet for den angivne konto."));
            }

            return Ok(u);
        }
    }
}
