﻿using Microsoft.Azure.CognitiveServices.Vision.Face.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebAPI.DAL;
using WebAPI.Helpers;
using WebAPI.Models;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    public class FaceController : ApiController
    {
        private UnitOfWork uow = new UnitOfWork();

        [HttpPost]
        [Route("compare")]
        public async Task<IHttpActionResult> FaceComparison()
        {
            try
            {
                var fileUploadPath = HttpContext.Current.Server.MapPath("~/TemporaryFiles");
                var multiFormDataStreamProvider = new MultiFileUploadProvider(fileUploadPath);

                await Request.Content.ReadAsMultipartAsync(multiFormDataStreamProvider);

                string uploadingFileName = multiFormDataStreamProvider.FileData.Select(x => x.LocalFileName).FirstOrDefault();

                NursingHome nursingHome = await GetNursingHome();
                string nursingHomeName = nursingHome.Name.Replace(" ", "").ToLower();

                Person matchingPerson = await new FaceAPIClient().CompareImage(uploadingFileName, nursingHomeName);

                Resident matchingResident = null;
                if (matchingPerson != null)
                {
                    var allResidents = await uow.ResidentRepository.Get(r => r.NursingHome.Id == nursingHome.Id, includeProperties: "ContactGroup, ContactGroup.Contacts, Contact");
                    matchingResident = allResidents.FirstOrDefault(r => r.Id.ToString() == matchingPerson.Name);
                }

                if (matchingResident.ContactGroup != null)
                {
                    var contacts = matchingResident.ContactGroup.Contacts;
                    string resident = matchingResident.Name + " fra værelse " + matchingResident.Room;
                    foreach (var contact in contacts)
                    {
                        var response = NotifyClient.Notify(resident, contact.Email, contact.Name);
                    }
                }

                if (matchingResident.Contact != null)
                {
                    string resident = matchingResident.Name + " fra værelse " + matchingResident.Room;
                    var response = NotifyClient.Notify(resident, matchingResident.Contact.Email, matchingResident.Contact.Name);
                }

                File.Delete(uploadingFileName);

                return Ok(matchingResident);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }

        }

        public async Task<NursingHome> GetNursingHome()
        {
            return await Task.Run(() => uow.NursingHomeRepository.dbSet.FirstOrDefault(x => x.User.Email == User.Identity.Name));
        }
    }
}
