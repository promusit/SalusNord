﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class ApplicationUser : IdentityUser
    {
        public virtual Provider Provider { get; set; }
        public virtual NursingHome NursingHome { get; set; }

    }
}