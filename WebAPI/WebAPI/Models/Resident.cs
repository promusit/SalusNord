﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class Resident
    {
        [Key]
        public int  Id { get; set; }
        public string Name { get; set; }
        public string Room { get; set; }
        public NursingHome NursingHome { get; set; }
        public ContactGroup ContactGroup { get; set; }
        public Contact Contact { get; set; }
    }
}