﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class ResidentViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Room { get; set; }
        public ContactGroupViewModel ContactGroup { get; set; }
        public ContactViewModel Contact { get; set; }
    }
}