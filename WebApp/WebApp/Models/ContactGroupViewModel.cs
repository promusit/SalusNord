﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class ContactGroupViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ContactViewModel> Contacts { get; set; }
    }
}