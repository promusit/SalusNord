﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class ZipViewModel
    {
        [Required, Microsoft.Web.Mvc.FileExtensions(Extensions = "zip", ErrorMessage = "Vælg venligst en zip fil.")]
        public HttpPostedFileBase File { get; set; }
    }
}