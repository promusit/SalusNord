﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Filters
{
    public class IsTokenValid : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            DateTime expiration = (DateTime)filterContext.HttpContext.Session["TokenExpires"];
            HttpResponseMessage msg = new HttpResponseMessage();

            if (expiration > DateTime.Now)
            {
                filterContext.HttpContext.Response.StatusCode = 200;
            }
            else
            {
                filterContext.HttpContext.Session["AccessToken"] = null;
                filterContext.HttpContext.Session["TokenExpires"] = null;
                //filterContext.HttpContext.Response.Status = "Session er udløbet. Login";
                filterContext.HttpContext.Response.Redirect("/");
            }
        }
    }

    internal class Http403Result : ActionResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.StatusCode = 403;
        }
    }
}