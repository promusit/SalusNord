﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApp.Helpers;
using WebApp.Helpers.Toast;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class ContactGroupsController : Controller
    {
        // GET: ContactGroups
        public async Task<ActionResult> Index()
        {
            var groups = await APIClient.GetAllGroups(Session["AccessToken"].ToString());
            return View(groups);
        }

        // GET: ContactGroups/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                var group = APIClient.GetGroup(Session["AccessToken"].ToString(), id);

                return View(group);

            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl!", e.Message, ToastType.Error);

                return View();
            }
        }

        // GET: ContactGroups/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ContactGroups/Create
        [HttpPost]
        public ActionResult Create(ContactGroupViewModel model)
        {
            try
            {
                var res = APIClient.CreateGroup(Session["AccessToken"].ToString(), model);

                if (res.StatusCode == HttpStatusCode.OK)
                {
                    this.AddToastMessage("Succes", "Medarbejderen blev oprettet.", ToastType.Success);
                }

                return RedirectToAction("Index");
            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl", e.Message, ToastType.Error);

                return View(model);
            }
        }

        // GET: ContactGroups/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                var group = APIClient.GetGroup(Session["AccessToken"].ToString(), id);
                return View(group);
            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl", e.Message, ToastType.Error);

                return View();
            }
        }

        // POST: ContactGroups/Edit/5
        [HttpPut]
        public ActionResult Edit(int id, ContactGroupViewModel model)
        {
            try
            {
                var res = APIClient.EditGroup(Session["AccessToken"].ToString(), id, model);

                if (res.StatusCode == HttpStatusCode.OK)
                {
                    this.AddToastMessage("Succes", "Medarbejderens oplysninger er blevet ændret.", ToastType.Success);
                }

                return RedirectToAction("Index");
            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl", e.Message, ToastType.Error);

                return View(model);
            }
        }

        // GET: ContactGroups/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                var group = APIClient.GetGroup(Session["AccessToken"].ToString(), id);

                return View(group);
            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl", e.Message, ToastType.Error);

                return View();
            }
        }

        // POST: ContactGroups/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, ContactGroupViewModel model)
        {
            try
            {
                var res = APIClient.DeleteGroup(Session["AccessToken"].ToString(), id);

                if (res.StatusCode == HttpStatusCode.OK)
                {
                    this.AddToastMessage("Succes!", "Kontaktgruppen er blevet slettet.", ToastType.Success);
                }

                return RedirectToAction("Index");
            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl", e.Message, ToastType.Error);

                return View(model);
            }
        }
    }
}
