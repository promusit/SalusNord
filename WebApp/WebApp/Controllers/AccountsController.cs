﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Helpers;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class AccountsController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            string token = (string)Session["AccessToken"];

            try
            {
                AccountViewModel model = LoginClient.GetAccountDetails(token);
                Session["Username"] = model.Name;
                Session["Role"] = model.Role;

                return View();
            }
            catch (HttpException e)
            {
                Session["Username"] = "FEJL";
                Session["Role"] = "FEJL";
                ViewBag.ErrorMessage = e.Message;

                return View();
            }
        }
    }
}