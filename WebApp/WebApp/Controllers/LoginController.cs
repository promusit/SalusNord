﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Helpers;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var tokenForSession = LoginClient.Login(model.Email, model.Password);

            if (tokenForSession?.Token == null)
            {
                var err = "Ugyldig kombination af email og password.";
                ModelState.AddModelError("", err);
                return View(model);
            }

            Session["AccessToken"] = tokenForSession.Token;
            Session["TokenExpires"] = tokenForSession.Expires;
            Session["LoggedIn"] = true;

            return RedirectToAction("Index", "Accounts");
        }

        [HttpGet]
        public ActionResult LogOut()
        {
            Session["AccessToken"] = null;
            Session["TokenExpires"] = null;
            Session["LoggedIn"] = false;
            Session["Username"] = null;
            Session["Role"] = null;

            return RedirectToAction("Index", "Home");
        }
    }
}