﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;
using WebApp.Filters;
using System.IO;
using WebApp.Helpers;
using System.Threading.Tasks;
using WebApp.Helpers.Toast;
using System.Net;

namespace WebApp.Controllers
{
    [IsTokenValid]
    public class ResidentsController : Controller
    {
        // GET: Residents
        public async Task<ActionResult> Index(string errorMessage = "")
        {
            var contactGroups = await APIClient.GetAllGroups(Session["AccessToken"].ToString());
            var contacts = await APIClient.GetAllContacts(Session["AccessToken"].ToString());

            var contactList = new List<SelectListItem>()
            {
                new SelectListItem { Text = "Ingen", Value = "Ingen" }
            };

            foreach (var group in contactGroups)
            {
                contactList.Add(new SelectListItem
                {
                    Text = "Gruppe: " + group.Name,
                    Value = "Gruppe-" + group.Id.ToString() + "-" + group.Name
                });
            }
            foreach (var contact in contacts)
            {
                contactList.Add(new SelectListItem
                {
                    Text = contact.Name,
                    Value = "Kontakt-" + contact.Id.ToString() + "-" + contact.Name
                });
            }
            ViewBag.contactList = contactList;

            var list = await APIClient.GetAllResidents(Session["AccessToken"].ToString());

            return View(list);
        }

        [HttpPost]
        public ActionResult Index(int id, string name, string room, string contactList = "")
        {
            try
            {
                ResidentViewModel model = new ResidentViewModel
                {
                    Id = id,
                    Name = name,
                    Room = room
                };

                if (id != 0)
                {
                    if (contactList != null && contactList != "")
                    {
                        string[] contactInfo = contactList.Split('-');
                        string contactType = contactInfo[0];
                        string contactId = contactInfo[1];
                        string contactName = contactInfo[2];

                        if (contactType == "Gruppe")
                        {
                            model.ContactGroup = new ContactGroupViewModel { Name = contactName };
                            model.Contact = null;
                        }

                        if (contactType == "Kontakt")
                        {
                            model.ContactGroup = null;
                            model.Contact = new ContactViewModel { Name = contactName };
                        }
                    }
                }

                try
                {
                    var res = APIClient.EditResident(Session["AccessToken"].ToString(), id, model);

                    if (res.StatusCode == HttpStatusCode.OK)
                    {
                        this.AddToastMessage("Succes", "Produktet er blevet fjernet fra sin gruppe.", ToastType.Success);
                    }
                }
                catch (HttpException e)
                {
                    this.AddToastMessage("Fejl", e.Message, ToastType.Error);
                    return RedirectToAction("Index");
                }

                return RedirectToAction("Index");

            }
            catch (HttpException e)
            {
                return RedirectToAction("Index", new { errorMessage = e.Message });
            }
        }

        // GET: Residents/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                var resident = APIClient.GetResident(Session["AccessToken"].ToString(), id);

                return View(resident);

            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl!", e.Message, ToastType.Error);

                return View();
            }
        }

        // GET: Residents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Residents/Create
        [HttpPost]
        public async Task<ActionResult> Create(HttpPostedFileBase file)
        {
            string fileName = String.Empty;
            string path = String.Empty;

            try
            {
                if (file.ContentLength > 0)
                {
                    fileName = Path.GetFileName(file.FileName);
                    path = Path.Combine(Server.MapPath("~/TemporaryFiles/"), fileName);

                    file.SaveAs(path);
                }

                string result = await APIClient.UploadResidents(Session["AccessToken"].ToString(), path);

                System.IO.File.Delete(path);

                if (result.Equals("\"Succeeded\""))
                {
                    return RedirectToAction("Index", "Residents");

                }

                ViewBag.Message = "Upload failed!";
                return View();

            }
            catch (Exception)
            {
                ViewBag.Message = "Upload failed!";
                return View();
            }
        }

        // GET: Residents/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                var resident = APIClient.GetResident(Session["AccessToken"].ToString(), id);
                return View(resident);
            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl", e.Message, ToastType.Error);

                return View();
            }
        }

        // POST: Residents/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ResidentViewModel model)
        {
            try
            {
                var res = APIClient.EditResident(Session["AccessToken"].ToString(), id, model);

                if (res.StatusCode == HttpStatusCode.OK)
                {
                    this.AddToastMessage("Succes", "Beboeren oplysninger er blevet ændret.", ToastType.Success);
                }

                return RedirectToAction("Index");
            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl", e.Message, ToastType.Error);

                return View(model);
            }
        }

        // GET: Residents/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                var contact = APIClient.GetResident(Session["AccessToken"].ToString(), id);

                return View(contact);
            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl", e.Message, ToastType.Error);

                return View();
            }
        }

        // POST: Residents/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, ResidentViewModel model)
        {
            try
            {
                var res = APIClient.DeleteResident(Session["AccessToken"].ToString(), id);

                if (res.StatusCode == HttpStatusCode.OK)
                {
                    this.AddToastMessage("Succes!", "Beboeren er blevet slettet.", ToastType.Success);
                }

                return RedirectToAction("Index");
            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl", e.Message, ToastType.Error);

                return View();
            }
        }
    }
}
