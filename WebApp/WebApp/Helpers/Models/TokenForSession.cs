﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Helpers.Models
{
    public class TokenForSession
    {
        public string Token { get; set; }
        public string TokenType { get; set; }
        public DateTime Expires { get; set; }
    }
}