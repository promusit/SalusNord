﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Helpers.Models
{
    public class ContactsResponse
    {
        public int id { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public ContactGroupsResponse contactGroup { get; set; }
    }
}