﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Helpers.Models
{
    public class ResidentsResponse
    {
        public int id { get; set; }
        public string name { get; set; }
        public string room { get; set; }
        public ContactGroupsResponse contactGroup { get; set; }
        public ContactsResponse contact { get; set; }
    }
}