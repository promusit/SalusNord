﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using WebApp.Helpers.Models;
using WebApp.Models;

namespace WebApp.Helpers
{
    public static class APIClient
    {
        private static RestClient client = new RestClient();
        private static readonly Uri baseUrl = new Uri("http://localhost:23362/{call}");


        #region Residents
        public static async Task<IEnumerable<ResidentViewModel>> GetAllResidents(string token)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.GET;
            request.AddUrlSegment("call", $"residents/all");

            var response = client.Execute<List<ResidentsResponse>>(request);
            if (response?.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }

            var residents = new List<ResidentViewModel>();
            foreach (var resident in response.Data)
            {
                ContactGroupViewModel group = null;
                ContactViewModel contact = null;

                if (resident.contactGroup != null)
                {
                    group = new ContactGroupViewModel
                    {
                        Id = resident.contactGroup.id,
                        Name = resident.contactGroup.name
                    };
                }

                if (resident.contact != null)
                {
                    contact = new ContactViewModel
                    {
                        Id = resident.contact.id,
                        Name = resident.contact.name
                    };
                }

                residents.Add(new ResidentViewModel
                {
                    Id = resident.id,
                    Name = resident.name,
                    Room = resident.room,
                    ContactGroup = group,
                    Contact = contact
                });
            }

            return residents;
        }

        public static ResidentViewModel GetResident(string token, int id)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.GET;
            request.AddUrlSegment("call", $"residents/{id.ToString()}");

            var response = client.Execute<ResidentsResponse>(request);

            if (response?.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }

            var resident = response.Data;

            return new ResidentViewModel
            {
                Id = resident.id,
                Name = resident.name,
                Room = resident.room
            };
        }

        public static async Task<String> UploadResidents(string token, string filePath)
        {
            HttpResponseMessage response;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:23362/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

                MultipartFormDataContent content = new MultipartFormDataContent();
                ByteArrayContent fileContent = new ByteArrayContent(File.ReadAllBytes(filePath));
                fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = Path.GetFileName(filePath) };
                content.Add(fileContent);

                response = await client.PostAsync("residents", content);
            }

            return await response.Content.ReadAsStringAsync();

        }

        public static IRestResponse<HttpResponseMessage> EditResident(string token, int id, ResidentViewModel model)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.PUT;
            request.AddUrlSegment("call", $"residents/{id.ToString()}");

            request.AddParameter("id", model.Id);
            request.AddParameter("name", model.Name);
            request.AddParameter("room", model.Room);


            if (model.ContactGroup == null && model.Contact == null)
            {
                request.AddParameter("update", "false");
                request.AddParameter("group", null);
                request.AddParameter("contact", null);
            }
            else
            {
                request.AddParameter("update", "true");
                if (model.ContactGroup != null)
                {
                    request.AddParameter("group", model.ContactGroup.Name);
                }
                else
                {
                    request.AddParameter("group", null);
                }

                if (model.Contact != null)
                {
                    request.AddParameter("contact", model.Contact.Name);
                }
                else
                {
                    request.AddParameter("contact", null);
                }
            }

            var response = client.Execute<HttpResponseMessage>(request);

            if (response?.StatusCode == HttpStatusCode.InternalServerError
                || response?.StatusCode == HttpStatusCode.Conflict)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }

            return response;
        }

        public static IRestResponse<HttpResponseMessage> DeleteResident(string token, int id)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.DELETE;
            request.AddUrlSegment("call", $"residents/{id.ToString()}");

            var response = client.Execute<HttpResponseMessage>(request);

            if (response?.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }

            return response;
        }

        #endregion

        #region Contacts
        public static async Task<IEnumerable<ContactViewModel>> GetAllContacts(string token)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.GET;
            request.AddUrlSegment("call", $"contacts/all");

            var response = client.Execute<List<ContactsResponse>>(request);
            if (response?.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }


            var contacts = new List<ContactViewModel>();
            foreach (var contact in response.Data)
            {
                if (contact.contactGroup != null)
                {
                    contacts.Add(new ContactViewModel
                    {
                        Id = contact.id,
                        Email = contact.email,
                        Name = contact.name,
                        Phone = contact.phone,
                        ContactGroup = new ContactGroupViewModel { Id = contact.contactGroup.id, Name = contact.contactGroup.name }

                    });
                }
                else
                {
                    contacts.Add(new ContactViewModel
                    {
                        Id = contact.id,
                        Email = contact.email,
                        Name = contact.name,
                        Phone = contact.phone,
                       
                    });
                }
            }


            return contacts;
        }

        public static ContactViewModel GetContact(string token, int id)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.GET;
            request.AddUrlSegment("call", $"contacts/{id}");

            var response = client.Execute<ContactsResponse>(request);

            if (response?.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }

            var contact = response.Data;

            return new ContactViewModel
            {
                Id = contact.id,
                Name = contact.name,
                Email = contact.email,
                Phone = contact.phone
            };
        }

        public static IRestResponse<HttpResponseMessage> CreateContact(string token, ContactViewModel model)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.POST;
            request.AddUrlSegment("call", "contacts");

            request.AddParameter("email", model.Email);
            request.AddParameter("name", model.Name);
            request.AddParameter("phone", model.Phone);

            var response = client.Execute<HttpResponseMessage>(request);

            if (response?.StatusCode == HttpStatusCode.Conflict
                || response?.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }

            return response;
        }

        public static IRestResponse<HttpResponseMessage> EditContact(string token, int id, ContactViewModel model, string groupId)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.PUT;
            request.AddUrlSegment("call", $"contacts/{id}");

            request.AddParameter("name", model.Name);
            request.AddParameter("email", model.Email);
            request.AddParameter("phone", model.Phone);
            request.AddParameter("group", groupId);

            var response = client.Execute<HttpResponseMessage>(request);

            if (response?.StatusCode == HttpStatusCode.InternalServerError
                || response?.StatusCode == HttpStatusCode.Conflict)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }

            return response;
        }

        public static IRestResponse<HttpResponseMessage> DeleteContact(string token, int id)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.DELETE;
            request.AddUrlSegment("call", $"contacts/{id}");

            var response = client.Execute<HttpResponseMessage>(request);

            if (response?.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }

            return response;
        }
        #endregion

        #region Groups
        public static async Task<IEnumerable<ContactGroupViewModel>> GetAllGroups(string token)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.GET;
            request.AddUrlSegment("call", $"groups/all");

            var response = client.Execute<List<ContactGroupsResponse>>(request);
            if (response?.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }

            var groups = new List<ContactGroupViewModel>();
            foreach (var group in response.Data)
            {
                var contacts = new List<ContactViewModel>();
                if (group.contacts != null)
                {
                    foreach (var contact in group.contacts)
                    {
                        contacts.Add(new ContactViewModel
                        {
                            Id = contact.id,
                            Name = contact.name,
                            Email = contact.email,
                            Phone = contact.phone
                        });
                    }
                }

                groups.Add(new ContactGroupViewModel
                {
                    Id = group.id,
                    Name = group.name,
                    Contacts = contacts
                });
            }

            return groups;
        }

        public static ContactGroupViewModel GetGroup(string token, int id)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.GET;
            request.AddUrlSegment("call", $"groups/{id}");

            var response = client.Execute<ContactGroupsResponse>(request);

            if (response?.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }

            var group = response.Data;

            var contacts = new List<ContactViewModel>();
            if (group.contacts != null)
            {
                foreach (var contact in group.contacts)
                {
                    contacts.Add(new ContactViewModel
                    {
                        Id = contact.id,
                        Name = contact.name,
                        Email = contact.email,
                        Phone = contact.phone
                    });
                }
            }

            return new ContactGroupViewModel
            {
                Id = group.id,
                Name = group.name,
                Contacts = contacts
            };
        }

        public static IRestResponse<HttpResponseMessage> CreateGroup(string token, ContactGroupViewModel model)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.POST;
            request.AddUrlSegment("call", "groups");

            request.AddParameter("name", model.Name);

            var response = client.Execute<HttpResponseMessage>(request);

            if (response?.StatusCode == HttpStatusCode.Conflict
                || response?.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }

            return response;
        }

        public static IRestResponse<HttpResponseMessage> EditGroup(string token, int id, ContactGroupViewModel model)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.PUT;
            request.AddUrlSegment("call", $"groups/{id}");

            request.AddParameter("name", model.Name);

            var response = client.Execute<HttpResponseMessage>(request);

            if (response?.StatusCode == HttpStatusCode.InternalServerError
                || response?.StatusCode == HttpStatusCode.Conflict)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }

            return response;
        }

        public static IRestResponse<HttpResponseMessage> DeleteGroup(string token, int id)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.DELETE;
            request.AddUrlSegment("call", $"groups/{id}");

            var response = client.Execute<HttpResponseMessage>(request);

            if (response?.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }

            return response;
        }
        #endregion


        private static string ParseErrorMessage(string message)
        {
            return JObject.Parse(message).SelectToken("message").ToString();
        }
    }
}